/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object DatabaseMigrations {
    private val MIGRATE_TO_V2 = object: Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            // the structure was changed and it's no big loss
            database.execSQL("DELETE FROM saved_task")
        }
    }

    private val MIGRATE_TO_V3 = object: Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE `user` ADD COLUMN `play_time` INTEGER NOT NULL DEFAULT 0")
        }
    }

    private val MIGRATE_TO_V4 = object: Migration(3, 4) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `statistic_task_result` (`user_id` INTEGER NOT NULL, `task_index` INTEGER NOT NULL, `type` TEXT NOT NULL, `correct` INTEGER NOT NULL, PRIMARY KEY(`user_id`, `task_index`))")
            database.execSQL("ALTER TABLE `user` ADD COLUMN `solved_tasks` INTEGER NOT NULL DEFAULT 0")
        }
    }

    private val MIGRATE_TO_V5 = object: Migration(4, 5) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("DROP TABLE `statistic_task_result`")
            database.execSQL("CREATE TABLE IF NOT EXISTS `statistic_task_result` (`user_id` INTEGER NOT NULL, `task_index` INTEGER NOT NULL, `type` TEXT NOT NULL, `correct` INTEGER NOT NULL, `duration` INTEGER NOT NULL, PRIMARY KEY(`user_id`, `task_index`))")
        }
    }

    private val MIGRATE_TO_V6 = object: Migration(5, 6) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE `user` ADD COLUMN `input_configuration` TEXT NOT NULL DEFAULT '{}'")
        }
    }

    private val MIGRATE_TO_V7 = object: Migration(6, 7) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE `saved_task` RENAME TO `saved_task_old`")
            database.execSQL("ALTER TABLE `statistic_task_result` RENAME TO `statistic_task_result_old`")

            database.execSQL("CREATE TABLE IF NOT EXISTS `saved_task` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `user_id` INTEGER NOT NULL, `task` TEXT NOT NULL, FOREIGN KEY(`user_id`) REFERENCES `user`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )")
            database.execSQL("CREATE TABLE IF NOT EXISTS `statistic_task_result` (`user_id` INTEGER NOT NULL, `task_index` INTEGER NOT NULL, `type` TEXT NOT NULL, `correct` INTEGER NOT NULL, `duration` INTEGER NOT NULL, PRIMARY KEY(`user_id`, `task_index`), FOREIGN KEY(`user_id`) REFERENCES `user`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )")

            database.execSQL("INSERT INTO saved_task (id, user_id, task) SELECT id, user_id, task FROM saved_task_old WHERE user_id IN (SELECT id FROM user)")
            database.execSQL("INSERT INTO statistic_task_result (user_id, task_index, type, correct, duration) SELECT user_id, task_index, type, correct, duration FROM statistic_task_result_old WHERE user_id IN (SELECT id FROM user)")

            database.execSQL("DROP TABLE `saved_task_old`")
            database.execSQL("DROP TABLE `statistic_task_result_old`")
        }
    }

    private val MIGRATE_TO_V8 = object: Migration(7, 8) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE INDEX IF NOT EXISTS `index_saved_task_user_id` ON `saved_task` (`user_id`)")
        }
    }

    val ALL = arrayOf(
        MIGRATE_TO_V2,
        MIGRATE_TO_V3,
        MIGRATE_TO_V4,
        MIGRATE_TO_V5,
        MIGRATE_TO_V6,
        MIGRATE_TO_V7,
        MIGRATE_TO_V8
    )
}