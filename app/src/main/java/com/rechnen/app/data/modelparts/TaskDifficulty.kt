/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter
import com.rechnen.app.extension.nextIntInRange
import com.rechnen.app.extension.nextIntWithDigits
import com.rechnen.app.extension.randomItem
import java.util.*

data class TaskDifficulty(
        val roundConfig: RoundConfig = RoundConfig(),
        val multiplication: MultiplicationConfig = MultiplicationConfig(),
        val addition: AdditionConfig = AdditionConfig(),
        val subtraction: SubtractionConfig = SubtractionConfig(),
        val factorization: FactorizationConfig = FactorizationConfig(),
        val division: DivisionConfig = DivisionConfig()
) {
    companion object {
        private const val ROUND_CONFIG = "roundConfig"
        private const val MULTIPLICATION = "multiplication"
        private const val ADDITION = "addition"
        private const val SUBTRACTION = "subtraction"
        private const val FACTORIZATION = "factorization"
        private const val DIVISION = "division"

        fun parse(reader: JsonReader): TaskDifficulty {
            var roundConfig: RoundConfig? = null
            var multiplication: MultiplicationConfig? = null
            var addition: AdditionConfig? = null
            var subtraction: SubtractionConfig? = null
            var factorization: FactorizationConfig? = null
            var division: DivisionConfig? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ROUND_CONFIG -> roundConfig = RoundConfig.parse(reader)
                    MULTIPLICATION -> multiplication = MultiplicationConfig.parse(reader)
                    ADDITION -> addition = AdditionConfig.parse(reader)
                    SUBTRACTION -> subtraction = SubtractionConfig.parse(reader)
                    FACTORIZATION -> factorization = FactorizationConfig.parse(reader)
                    DIVISION -> division = DivisionConfig.parse(reader)
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return TaskDifficulty(
                    roundConfig = roundConfig!!,
                    multiplication = multiplication!!,
                    addition = addition!!,
                    subtraction = subtraction!!,
                    // these were added later => default values are required
                    factorization = factorization ?: FactorizationConfig(),
                    division = division ?: DivisionConfig()
            )
        }
    }

    val enabledTaskTypes: List<TaskType> by lazy {
        val result = mutableListOf<TaskType>()

        if (multiplication.enable) { result.add(TaskType.Multiplication) }
        if (addition.enable) { result.add(TaskType.Addition) }
        if (subtraction.enable) { result.add(TaskType.Subtraction) }
        if (factorization.enable) { result.add(TaskType.Factorization) }
        if (division.enable) { result.add(TaskType.Division) }

        result
    }

    fun generateTask(random: Random): Task = generateTask(random, enabledTaskTypes.randomItem(random))

    private fun generateTask(random: Random, operator: TaskType): Task = when (operator) {
        TaskType.Multiplication -> Task.BinaryTask.MultiplicationTask(
                left = random.nextIntWithDigits(multiplication.digitsOfFirstNumber).toLong(),
                right = random.nextIntWithDigits(multiplication.digitsOfSecondNumber).toLong()
        )
        TaskType.Addition -> {
            val left = random.nextIntWithDigits(addition.digitsOfFirstNumber)

            val right = if (addition.enableTenTransgression) {
                random.nextIntWithDigits(addition.digitsOfSecondNumber)
            } else {
                random.nextIntInRange(1, 10 - left % 10)
            }

            Task.BinaryTask.AdditionTask(
                    left = left.toLong(),
                    right = right.toLong()
            )
        }
        TaskType.Subtraction -> {
            val left = random.nextIntWithDigits(subtraction.digitsOfFirstNumber)

            val right = if (!subtraction.enableTenTransgression) {
                random.nextIntInRange(1, left % 10)
            } else {
                random.nextIntWithDigits(subtraction.digitsOfSecondNumber)
            }

            if (left < right && !subtraction.allowNegativeResults)
                Task.BinaryTask.SubtractionTask(
                        left = right.toLong(),
                        right = left.toLong()
                )
            else
                Task.BinaryTask.SubtractionTask(
                        left = left.toLong(),
                        right = right.toLong()
                )
        }
        TaskType.Factorization -> Task.MultiSetTask.FactorizationTask(random.nextIntInRange(factorization.smallestNumber, factorization.biggestNumber).toLong())
        TaskType.Division -> {
            if (division.withRemainder) {
                val left = random.nextIntInRange(0, division.maxDividend)
                val right = random.nextIntInRange(1, division.maxDivisor)

                Task.DivisionTask(
                        left = left.toLong(),
                        right = right.toLong(),
                        withRemainder = true
                )
            } else {
                // a * b = c
                // a = c / b

                // precondition: b <= maxDivisor
                // goal: a * b = c <= maxDividend
                // a * b <= maxDividend
                // a <= maxDividend / b

                val b = random.nextIntInRange(1, division.maxDivisor)
                val a = random.nextIntInRange(0, division.maxDividend / b)
                val c = a * b

                Task.DivisionTask(
                        left = c.toLong(),
                        right = b.toLong(),
                        withRemainder = false
                )
            }
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ROUND_CONFIG); roundConfig.serialize(writer)
        writer.name(MULTIPLICATION); multiplication.serialize(writer)
        writer.name(ADDITION); addition.serialize(writer)
        writer.name(SUBTRACTION); subtraction.serialize(writer)
        writer.name(FACTORIZATION); factorization.serialize(writer)
        writer.name(DIVISION); division.serialize(writer)

        writer.endObject()
    }
}