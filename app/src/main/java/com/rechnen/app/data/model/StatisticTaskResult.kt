/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.model

import androidx.room.*
import com.rechnen.app.data.converter.TaskTypeConverter
import com.rechnen.app.data.modelparts.TaskType

@Entity(
        tableName = "statistic_task_result",
        primaryKeys = ["user_id", "task_index"],
        foreignKeys = [
                ForeignKey(
                        entity = User::class,
                        parentColumns = ["id"],
                        childColumns = ["user_id"],
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE
                )
        ]
)
@TypeConverters(TaskTypeConverter::class)
data class StatisticTaskResult(
        @ColumnInfo(name = "user_id")
        val userId: Int,
        @ColumnInfo(name = "task_index")
        val taskIndex: Long,
        val type: TaskType,
        val correct: Boolean,
        val duration: Long
)

@TypeConverters(TaskTypeConverter::class)
data class AggregatedStatisticTaskResult(
        val type: TaskType,
        val total: Int,
        val correct: Int
)