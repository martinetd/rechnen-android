/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter

data class AdditionConfig(
        val enable: Boolean = true,
        val digitsOfFirstNumber: Int = 2,
        val digitsOfSecondNumber: Int = 2,   // not required if enableTenTransgression = false
        val enableTenTransgression: Boolean = true
) {
    companion object {
        private const val ENABLE = "enable"
        private const val DIGITS_OF_FIRST_NUMBER = "digitsOfFirstNumber"
        private const val DIGITS_OF_SECOND_NUMBER = "digitsOfSecondNumber"
        private const val ENABLE_TEN_TRANSGRESSION = "enableTenTransgression"

        fun parse(reader: JsonReader): AdditionConfig {
            var enable: Boolean? = null
            var digitsOfFirstNumber: Int? = null
            var digitsOfSecondNumber: Int? = null
            var enableTenTransgression: Boolean? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ENABLE -> enable = reader.nextBoolean()
                    DIGITS_OF_FIRST_NUMBER -> digitsOfFirstNumber = reader.nextInt()
                    DIGITS_OF_SECOND_NUMBER -> digitsOfSecondNumber = reader.nextInt()
                    ENABLE_TEN_TRANSGRESSION -> enableTenTransgression = reader.nextBoolean()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return AdditionConfig(
                    enable = enable!!,
                    digitsOfFirstNumber = digitsOfFirstNumber!!,
                    digitsOfSecondNumber = digitsOfSecondNumber!!,
                    enableTenTransgression = enableTenTransgression!!
            )
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ENABLE).value(enable)
        writer.name(DIGITS_OF_FIRST_NUMBER).value(digitsOfFirstNumber)
        writer.name(DIGITS_OF_SECOND_NUMBER).value(digitsOfSecondNumber)
        writer.name(ENABLE_TEN_TRANSGRESSION).value(enableTenTransgression)

        writer.endObject()
    }
}