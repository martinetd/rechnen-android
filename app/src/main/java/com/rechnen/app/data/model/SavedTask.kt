/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.model

import androidx.room.*
import com.rechnen.app.data.modelparts.Task

@Entity(
        tableName = "saved_task",
        foreignKeys = [
                ForeignKey(
                        entity = User::class,
                        parentColumns = ["id"],
                        childColumns = ["user_id"],
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE
                )
        ],
        indices = [
                Index(
                        value = ["user_id"]
                )
        ]
)
data class SavedTask(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        @ColumnInfo(name = "user_id")
        val userId: Int,
        val task: Task
)