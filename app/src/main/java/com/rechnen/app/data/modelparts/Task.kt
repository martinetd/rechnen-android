/*
 * Calculate Android Copyright <C> 2020 - 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter
import com.rechnen.app.ui.training.TaskView
import kotlin.math.absoluteValue

sealed class Task(val prefix: String?, val separator: String?, val maxListLength: Int, val type: TaskType, val params: List<Long>) {
    companion object {
        private const val TYPE = "type"
        private const val PARAMS = "params"

        fun parse(reader: JsonReader): Task {
            var type: TaskType? = null
            var params: List<Long>? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    TYPE -> type = TaskTypes.fromString(reader.nextString())
                    PARAMS -> params = mutableListOf<Long>().also { list ->
                        reader.beginArray()
                        while (reader.hasNext()) { list.add(reader.nextLong()) }
                        reader.endArray()
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return parse(type!!, params!!)
        }

        fun parse(type: TaskType, params: List<Long>): Task = when (type) {
            TaskType.Addition -> {
                if (params.size != 2) throw IllegalArgumentException()

                BinaryTask.AdditionTask(params[0], params[1])
            }
            TaskType.Subtraction -> {
                if (params.size != 2) throw IllegalArgumentException()

                BinaryTask.SubtractionTask(params[0], params[1])
            }
            TaskType.Multiplication -> {
                if (params.size != 2) throw IllegalArgumentException()

                BinaryTask.MultiplicationTask(params[0], params[1])
            }
            TaskType.Factorization -> MultiSetTask.FactorizationTask(params.single())
            TaskType.Division -> {
                if (params.size != 3) throw IllegalArgumentException()

                DivisionTask(
                        withRemainder = when (params[0]) {
                            0L -> false
                            1L -> true
                            else -> throw IllegalArgumentException()
                        },
                        left = params[1],
                        right = params[2]
                )
            }
        }
    }

    abstract val readableTask: String
    abstract val taskPresentation: List<TaskView.Line>
    abstract fun checkSolution(values: List<Long>, prefixSet: Boolean): Boolean

    init {
        if (maxListLength < 1 || (maxListLength > 1 && separator == null)) {
            throw IllegalArgumentException()
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(TYPE).value(TaskTypes.toString(type))
        writer.name(PARAMS).beginArray()
        params.forEach { writer.value(it) }
        writer.endArray()

        writer.endObject()
    }

    sealed class BinaryTask(
            val left: Long, val right: Long, val solutionValue: Long, val solutionPrefix: Boolean?,
            val symbol: String, prefix: String?, type: TaskType, params: List<Long>
    ): Task(prefix = prefix, maxListLength = 1, type = type, params = params, separator = null) {
        override val readableTask: String = left.toString() + symbol + right.toString()

        override val taskPresentation: List<TaskView.Line>
            get() = listOf(
                TaskView.Line(left = "", right = left.toString()),
                TaskView.Line(left = symbol, right = right.toString())
            )

        init {
            if (solutionValue < 0 || solutionPrefix != null && prefix == null) {
                throw IllegalArgumentException()
            }
        }

        override fun checkSolution(values: List<Long>, prefixSet: Boolean): Boolean {
            if (values.isEmpty()) return false
            if (values.size > 1) throw IllegalArgumentException()

            return values.single() == solutionValue && (solutionPrefix == null || prefixSet == solutionPrefix)
        }

        class AdditionTask(left: Long, right: Long): BinaryTask(
                prefix = null,
                symbol = "+",
                left = left,
                right = right,
                solutionValue = left + right,
                solutionPrefix = null,
                type = TaskType.Addition,
                params = listOf(left, right)
        )

        class SubtractionTask(left: Long, right: Long): BinaryTask(
                prefix = "-",
                symbol = "-",
                left = left,
                right = right,
                solutionValue = (left - right).absoluteValue,
                solutionPrefix = if (left == right) null else left - right < 0,
                type = TaskType.Subtraction,
                params = listOf(left, right)
        )

        class MultiplicationTask(left: Long, right: Long): BinaryTask(
                prefix = null,
                symbol = "*",
                left = left,
                right = right,
                solutionValue = left * right,
                solutionPrefix = null,
                type = TaskType.Multiplication,
                params = listOf(left, right)
        )
    }

    sealed class MultiSetTask(
            override val readableTask: String,
            private val solution: List<Long>,
            type: TaskType,
            separator: String,
            params: List<Long>
    ): Task(maxListLength = Int.MAX_VALUE, prefix = null, type = type, separator = separator, params = params) {
        init {
            if (solution.isEmpty()) throw IllegalArgumentException()
        }

        override fun checkSolution(values: List<Long>, prefixSet: Boolean): Boolean {
            if (values.size != solution.size) return false

            val solutionClone = solution.toMutableList()

            values.forEach { value ->
                val matchingSolutionIndex = solutionClone.indexOfFirst { it == value }

                if (matchingSolutionIndex == -1) return false

                solutionClone.removeAt(matchingSolutionIndex)
            }

            return true
        }

        class FactorizationTask(private val number: Long): MultiSetTask(
                readableTask = number.toString(),
                solution = primeFactors(number),
                type = TaskType.Factorization,
                separator = "*",
                params = listOf(number)
        ) {
            override val taskPresentation: List<TaskView.Line>
                get() = listOf(TaskView.Line(left = "", right = number.toString()))

            companion object {
                fun primeFactors(number: Long): List<Long> {
                    if (number < 1) throw IllegalArgumentException()
                    if (number == 1L) return listOf(1)

                    var input = number
                    var factor = 1L
                    val result = mutableListOf<Long>()

                    while (++factor <= input) {
                        while (input % factor == 0L) {
                            input /= factor
                            result.add(factor)
                        }
                    }

                    return result
                }
            }
        }
    }

    data class DivisionTask(val left: Long, val right: Long, val withRemainder: Boolean): Task(
            maxListLength = if (withRemainder) 2 else 1,
            prefix = null,
            type = TaskType.Division,
            separator = "R",
            params = listOf(
                    if (withRemainder) 1L else 0L,
                    left,
                    right
            )
    ) {
        init {
            if (left < 0 || right <= 0) {
                throw IllegalArgumentException()
            }

            if (left % right != 0L && !withRemainder) {
                throw IllegalArgumentException()
            }
        }

        override val readableTask: String = "$left/$right"

        override val taskPresentation: List<TaskView.Line>
            get() = listOf(
                TaskView.Line(left = "", right = left.toString()),
                TaskView.Line(left = "/", right = right.toString())
            )

        override fun checkSolution(values: List<Long>, prefixSet: Boolean): Boolean {
            if (values.size < 1 || values.size > 2 || prefixSet) return false
            if (values.size == 2 && !withRemainder) return false

            val inputNumber = values[0]
            val inputRemainder = if (values.size == 2) values[1] else 0

            return right * inputNumber + inputRemainder == left && inputRemainder < right
        }
    }
}