/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.extension

import java.util.*

fun <T> List<T>.randomItem(random: Random): T {
    if (this.isEmpty()) {
        throw IllegalArgumentException()
    } else if (this.size == 1) {
        return this.single()
    } else {
        return this[random.nextInt(this.size)]
    }
}

fun List<Int>.seekBarIndexOf(value: Int): Int {
    val index = this.indexOf(value)

    if (index != -1) {
        return index
    }

    val index2 = this.indexOfFirst { it > value }

    return if (index2 == -1) {
        this.size - 1
    } else if (index2 == 0) {
        0
    } else {
        index2 - 1
    }
}