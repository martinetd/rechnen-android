/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import com.rechnen.app.R
import kotlin.math.roundToInt
import kotlin.properties.Delegates


class TaskView(context: Context, attributeSet: AttributeSet): View(context, attributeSet) {
    var lines: Lines by Delegates.observable(Lines(emptyList())) { _, oldValue, newValue ->
        longestLineDirty = true
        usedFontSizeDirty = true
        update()

        if (oldValue.lines.size != newValue.lines.size) requestLayout()

        invalidate()
    }

    var expectedChars: String by Delegates.observable("0123456789+-/*") { _, _, _ ->
        maxLetterWidthDirty = true
        update()
        requestLayout()
    }

    private var maxFontSize = 16F
    private val paint = Paint()

    private var longestLine = 0
    private var longestLineDirty = true

    private var maxLetterWidth = 1F
    private var maxLetterWidthDirty = true

    private var usedFontSize = 1F
    private var usedFontSizeDirty = true
    private val scaledLetterWidth get() = maxLetterWidth * usedFontSize / maxFontSize

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val usableWidth = measuredWidth - paddingLeft - paddingRight

        paint.textSize = usedFontSize

        lines.lines.forEachIndexed { lineIndex, line ->
            val y = paddingTop + maxFontSize * (lineIndex + 1)

            val lineTooLong = line.totalLength * maxLetterWidth > usableWidth

            if (lineTooLong && line.ellipsize != null) {
                canvas.drawText(line.ellipsize, paddingLeft.toFloat(), y, paint)

                val remainingUsableWidth = usableWidth - paint.measureText(line.ellipsize)
                val remainingChars = (remainingUsableWidth / maxLetterWidth).toInt()

                val remainingLine = line.right.substring(
                    (line.right.length - remainingChars)
                        .coerceAtLeast(0)
                        .coerceAtMost(line.right.length - 1)
                )

                val x = measuredWidth - paddingRight - remainingLine.length * scaledLetterWidth

                remainingLine.forEachIndexed { charIndex, _ ->
                    canvas.drawText(remainingLine, charIndex, charIndex + 1, x + charIndex * scaledLetterWidth, y, paint)
                }
            } else {
                kotlin.run {
                    val x = paddingLeft

                    line.left.forEachIndexed { charIndex, _ ->
                        canvas.drawText(line.left, charIndex, charIndex + 1, x + charIndex * scaledLetterWidth, y, paint)
                    }
                }

                kotlin.run {
                    val x = measuredWidth - paddingRight - line.right.length * scaledLetterWidth

                    line.right.forEachIndexed { charIndex, _ ->
                        canvas.drawText(line.right, charIndex, charIndex + 1, x + charIndex * scaledLetterWidth, y, paint)
                    }
                }
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            getDefaultSize(suggestedMinimumWidth, widthMeasureSpec),
            MeasureSpec.makeMeasureSpec((paddingTop + paddingBottom + lines.lines.size * maxFontSize).roundToInt(), MeasureSpec.EXACTLY)
        )

        usedFontSizeDirty = true
        update()
    }

    private fun update() {
        paint.textSize = maxFontSize

        if (longestLineDirty) {
            val newLongestLine = lines.lines.maxOfOrNull { if (it.ellipsize == null) it.totalLength else 0 } ?: 0

            if (newLongestLine != longestLine) {
                longestLine = newLongestLine
                usedFontSizeDirty = true
            }

            longestLineDirty = false
        }

        if (maxLetterWidthDirty) {
            maxLetterWidth = expectedChars.mapIndexed { index, _ -> paint.measureText(expectedChars, index, index + 1) }.maxOrNull() ?: 0F
            maxLetterWidthDirty = false
            usedFontSizeDirty = true
        }

        if (usedFontSizeDirty) {
            val usableWidth = measuredWidth - paddingLeft - paddingRight

            usedFontSize = (maxFontSize * usableWidth / (longestLine * maxLetterWidth).coerceAtLeast(1F))
                .coerceAtMost(maxFontSize)
                .coerceAtLeast(4F)

            usedFontSizeDirty = false
        }
    }

    init {
        maxFontSize = context.resources.getDimension(R.dimen.veryLargeText)

        TypedValue().let { textAppearance ->
            context.theme.resolveAttribute(android.R.attr.textAppearanceLarge, textAppearance, true)

            context.obtainStyledAttributes(textAppearance.data, intArrayOf(android.R.attr.textColor)).also { params ->
                paint.color = params.getColor(0, Color.RED)
            }.recycle()
        }
    }

    init { update() }

    init {
        if (isInEditMode) {
            lines = Lines(listOf(
                Line("", "123"),
                Line("+", "456"),
                Line("=", "789")
            ))
        }
    }

    data class Line(val left: String, val right: String, val ellipsize: String? = null) { val totalLength = left.length + right.length }
    data class Lines(val lines: List<Line>)
}