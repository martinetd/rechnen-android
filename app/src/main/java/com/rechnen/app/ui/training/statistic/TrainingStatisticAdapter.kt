/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */

package com.rechnen.app.ui.training.statistic

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.rechnen.app.databinding.*
import kotlin.properties.Delegates

class TrainingStatisticAdapter: RecyclerView.Adapter<TrainingStatisticAdapter.ViewHolder>() {
    companion object {
        private const val HEADER = 1
        private const val DATA_ROW = 2
        private const val HEADLINE = 3
        private const val FOOTER = 4
        private const val TASK = 5
    }

    var items: List<TrainingStatisticListItem> by Delegates.observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = when (items[position]) {
        is TrainingStatisticListItem.TypeHeader -> HEADER
        is TrainingStatisticListItem.DataRow -> DATA_ROW
        is TrainingStatisticListItem.ResultInfoHeadline -> HEADLINE
        is TrainingStatisticListItem.ResultFooterText -> FOOTER
        is TrainingStatisticListItem.TaskInfo -> TASK
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = when (viewType) {
        HEADER -> ViewHolder(TrainingStatisticHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        DATA_ROW -> ViewHolder(TrainingStatisticRowBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        HEADLINE -> ViewHolder(TrainingResultHeadlineBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        FOOTER -> ViewHolder(TrainingResultFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        TASK -> ViewHolder(TrainingStatisticTaskBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        else -> throw IllegalArgumentException()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        when (item) {
            is TrainingStatisticListItem.TypeHeader -> TrainingStatisticViewBinder.bind(item = item, view = holder.binding as TrainingStatisticHeaderBinding)
            is TrainingStatisticListItem.DataRow -> TrainingStatisticViewBinder.bind(item = item, view = holder.binding as TrainingStatisticRowBinding)
            is TrainingStatisticListItem.ResultInfoHeadline -> { (holder.binding as TrainingResultHeadlineBinding).text.setText(item.text(holder.itemView.context)) }
            is TrainingStatisticListItem.ResultFooterText -> { (holder.binding as TrainingResultFooterBinding).text.setText(item.textRessourceId) }
            is TrainingStatisticListItem.TaskInfo -> TrainingStatisticViewBinder.bind(item = item, view = holder.binding as TrainingStatisticTaskBinding)
        }.let {/* require handling all paths */}
    }

    class ViewHolder(val binding: ViewBinding): RecyclerView.ViewHolder(binding.root)
}