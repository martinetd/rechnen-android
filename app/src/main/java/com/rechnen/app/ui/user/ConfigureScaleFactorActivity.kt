/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import com.rechnen.app.R
import com.rechnen.app.data.modelparts.InputConfiguration
import com.rechnen.app.databinding.ConfigureScaleFactorActivityBinding
import com.rechnen.app.extension.setOnChangeListener
import com.rechnen.app.extension.useJsonReader
import com.rechnen.app.extension.writeJson
import com.rechnen.app.ui.training.TaskView
import com.rechnen.app.ui.training.TrainingMainActivity

class ConfigureScaleFactorActivity: FragmentActivity() {
    companion object {
        const val EXTRA_INITIAL_CONFIG = "config"
        const val RESULT_EXTRA_FACTOR = "factor"

        fun buildIntent(context: Context, inputConfiguration: InputConfiguration) = Intent(context, ConfigureScaleFactorActivity::class.java)
                .putExtra(EXTRA_INITIAL_CONFIG, writeJson { inputConfiguration.serialize(it) })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ConfigureScaleFactorActivityBinding>(this, R.layout.configure_scale_factor_activity)
        val initalConfig = intent.getStringExtra(EXTRA_INITIAL_CONFIG)!!.useJsonReader { InputConfiguration.parse(it) }

        if (savedInstanceState == null) {
            binding.sizeSeekBar.progress = initalConfig.size / 10
        }

        TrainingMainActivity.applyConfigToKeyboard(initalConfig, binding.trainingActivity.keyboard)

        binding.sizeSeekBar.setOnChangeListener { binding.trainingActivity.keyboard.scaleFactor = it * 10 }

        binding.saveButton.setOnClickListener {
            setResult(RESULT_OK, Intent().putExtra(RESULT_EXTRA_FACTOR, binding.trainingActivity.keyboard.scaleFactor))
            finish()
        }

        binding.trainingActivity.keyboard.isEnabled = false
        binding.trainingActivity.task.lines = TaskView.Lines(listOf(
            TaskView.Line("", "123"),
            TaskView.Line("+", "456"),
            TaskView.Line("=", "579")
        ))
    }
}