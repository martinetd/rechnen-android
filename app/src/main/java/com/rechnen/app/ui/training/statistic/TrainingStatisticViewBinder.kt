/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */

package com.rechnen.app.ui.training.statistic

import android.content.Context
import com.rechnen.app.R
import com.rechnen.app.databinding.TrainingStatisticHeaderBinding
import com.rechnen.app.databinding.TrainingStatisticRowBinding
import com.rechnen.app.databinding.TrainingStatisticTaskBinding
import com.rechnen.app.ui.tasktype.TaskTypeInfo
import com.rechnen.app.ui.training.TrainingStatisticItemChange
import com.rechnen.app.ui.training.TrainingStatisticItemParameter
import com.rechnen.app.ui.training.TrainingStatisticItemSource

object TrainingStatisticViewBinder {
    fun bind(item: TrainingStatisticListItem.TypeHeader, view: TrainingStatisticHeaderBinding) {
        val context = view.root.context

        view.title.text = context.getString(
                when (item.parameter) {
                    TrainingStatisticItemParameter.AverageDurationPerCorrectTask -> R.string.statistic_average_duration_per_correct_task_at
                    TrainingStatisticItemParameter.PercentageOfCorrectlySolvedTasks ->  R.string.statistic_percentage_of_correctly_solved_tasks
                },
                context.getString(TaskTypeInfo.get(item.taskType).labelResourceId)
        )

        when (item.change) {
            TrainingStatisticItemChange.Better -> view.image.setImageResource(R.drawable.ic_baseline_trending_up_24)
            TrainingStatisticItemChange.Equal -> view.image.setImageResource(R.drawable.ic_baseline_trending_flat_24)
            TrainingStatisticItemChange.Worser -> view.image.setImageResource(R.drawable.ic_baseline_trending_down_24)
            null -> view.image.setImageDrawable(null)
        }
    }

    fun bind(item: TrainingStatisticListItem.DataRow, view: TrainingStatisticRowBinding) {
        val context = view.root.context

        val sourceLabel = context.getString(
                when (item.source) {
                    TrainingStatisticItemSource.BeforeThisSession -> R.string.statistic_source_before_this_session
                    TrainingStatisticItemSource.ThisSessionBeforeThisRound -> R.string.statistic_source_this_session_before_this_round
                    TrainingStatisticItemSource.ThisRound -> R.string.statistic_source_this_round
                }
        )

        val formattedValue = when (item.parameter) {
            TrainingStatisticItemParameter.AverageDurationPerCorrectTask -> formatSeconds(item.value.toLong(), context)
            TrainingStatisticItemParameter.PercentageOfCorrectlySolvedTasks -> "${item.value}%"
        }

        view.text.text = "${sourceLabel}: $formattedValue"

        view.progress.max = item.max
        view.progress.progress = item.value
    }

    fun bind(item: TrainingStatisticListItem.TaskInfo, view: TrainingStatisticTaskBinding) {
        view.text = "${item.task}=${item.input}"
        view.duration = formatSeconds(item.duration, view.root.context)
        view.flipper.displayedChild = if (item.correct) 1 else 0
    }

    private fun formatSeconds(milliseconds: Long, context: Context) = "${milliseconds / 1000}.${milliseconds / 100 % 10} ${context.getString(R.string.statistic_value_seconds)}"
}